# Partie 1

## Sous-partie 1: texte

Une phrase sans rien  

_Une phrase en italique_  

**Une phrase en gras**

Une lien vers [fun-mooc.fr](fun-mooc.fr)

Un ligne de code

## Sous-partie 2: listes

**Liste a puce**
* item    
    * Sous-item
    * Sous-item              
* item
* item 

**Liste numerotee**

1. item
2. item 
3. item 

## Sous-partie 3: code
